### MadFile

Making some madness with files!

-h -- show options discription;

-d -- set directory as argument, default: work directory;


-c -- create N (argument) files with random content;

-s -- size of files created in bytes (default: 4096);


-e -- erase all files in directory;

-t -- use separate threads (count is argument) for delete-operations;

-t arguments: 

0  = the same as no -t option;

1  = one additional thread with 2 buffers;

2+ = several threads with single buffer for each thread;


### Build:

just call "make" from ./src

or use QtCreator and .pro file

### Using:


create 100000 files with size 4k and random binary content in directory ./test:
```
#!bash

mf -d test -c 100000
```

create 100 files with size 16k and random binary content in current work directory:

```
#!bash

mf -c 100 -s 16384
```


remove all files from ./test directory using 4 eraser threads:

```
#!bash

mf -d test -e -t 4
```


remove all files (including application itself) from current directory using single thread:

```
#!bash

mf -e
```


etc.