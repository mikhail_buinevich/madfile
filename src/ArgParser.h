#ifndef ARGPARSER_H
#define ARGPARSER_H

#include <unistd.h>

#include <functional>
#include <sstream>
#include <map>

class ArgParser {
  typedef std::function< void ( const std::string& ) > func_type;

  struct ArgAttrs {
    func_type func;
    bool hasValue;
    std::string help;

    ArgAttrs() = default;
    ArgAttrs( bool _hasValue, const std::string& _crHelp, func_type _func ):
      func( _func ),
      hasValue( _hasValue ),
      help( _crHelp ) {}
  };

  std::map< char, ArgAttrs > table;

public:
  static const bool HAS_ARG = true;
  static const bool NO_ARG = false;

  void AddArg( char _token, bool _hasValue, const std::string& _crHelp, func_type _func ) {
    table[ _token ] = ArgAttrs( _hasValue, _crHelp, _func );
  }

  bool Parse( int _argc, char* _argv[] ) const {
    std::stringstream ss;
    for( auto item: table )
      ss << item.first << (item.second.hasValue ? ":" : "");

    int opt;
    try {
      while( (opt = getopt( _argc, _argv, ss.str().c_str() )) != -1 )
        table.at( opt ).func( optarg != 0 ? optarg : "" );
    }
    catch( std::out_of_range ) {
      PrintHelp();
      return false;
    }

    return true;
  }

  void PrintHelp() const {
    std::cout << "Possible options are:\n";
    for( auto item: table )
      std::cout << "\t-" << item.first << " -- " << item.second.help << "\n";
    std::cout << std::endl;
  }
};

#endif // ARGPARSER_H

