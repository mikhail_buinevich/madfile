#include "DirCtlr.h"

#include <iostream>
#include <sys/stat.h>
#include <dirent.h>
#include <stdio.h>
#include <fcntl.h>
#include <fstream>
#include <exception>
#include <thread>
#include <atomic>
#include <string.h>
#include <condition_variable>
#include <sstream>
#include <iomanip>
#include <vector>

DirCtrl::DirCtrl( const std::string& _crPath ):
  path( _crPath ),
  dp( nullptr ),
  ep( nullptr ) { }

DirCtrl::~DirCtrl() {
  closedir( dp );
}

bool DirCtrl::Exists() const {
  struct stat buffer;
  return stat( path.c_str(), &buffer ) != -1;
}

bool DirCtrl::TryMake() const {
  if( mkdir( path.c_str(), 0755 ) != 0 )
    return std::cerr << "Creating directory finished with error: "
                     << strerror( errno ) << std::endl, false;
  return true;
}

bool DirCtrl::Open() {
  dp = opendir( path.c_str() );
  if( dp == nullptr )
    return std::cerr << "Opening directory finished with error: "
                     << strerror( errno ) << std::endl, false;
  return true;
}

bool DirCtrl::NextFile() {
  return (ep = readdir( dp )) != nullptr;
}

std::string DirCtrl::NextFileName() {
  return (ep = readdir( dp )) != nullptr ? ep->d_name : "";
}

void DirCtrl::Reset() {
  rewinddir( dp );
}

bool DirCtrl::AtEnd() const {
  return ep == nullptr;
}

std::string DirCtrl::CurrentName() const {
  return ep != nullptr ? ep->d_name : "";
}

bool DirCtrl::RemoveCurrent() {
  return Remove( ep->d_name );
}

const std::string DirCtrl::MARKER = "#END";

bool DirCtrl::Remove( const std::string& _crName ) {
  std::string filePath = path + "/" + _crName;
  if( remove( filePath.c_str() ) != 0 and
      (not _crName.empty() and _crName != "." and _crName != ".." and _crName != MARKER) )
    return std::cerr << "\nCan not remove file " << _crName << ": "
                     << strerror( errno ) << "\n", false;
  return true;
}

size_t DirCtrl::RemoveAll() {
  static const size_t outputPeriod = 1000;
  static const size_t outputFieldWidth = 12;
  std::string backspaces( outputFieldWidth, '\b' );
  size_t count = 0;
  ep = reinterpret_cast< dirent* >( 1 );
  std::cout << "Files removed:" << std::setw( outputFieldWidth ) << 0 << std::flush;
  while( not AtEnd() ) {
    for( size_t i = 0; i < outputPeriod && NextFile(); i++ )
      count += RemoveCurrent();
    std::cout << backspaces << std::setw( outputFieldWidth ) << count << std::flush;
  }
  std::cout << backspaces << std::setw( outputFieldWidth ) << count << std::endl;
  return count;
}

size_t DirCtrl::RemoveAllT() {
  std::string buffer[2];
  std::mutex mx_read[2];
  mx_read[ 0 ].lock();
  mx_read[ 1 ].lock();
  std::mutex mx_proc[2];
  size_t count = 0;

  std::thread eraser( [ this, &buffer, &mx_read, &mx_proc, &count ](){
    static const size_t outputFieldWidth = 12;
    std::string backspaces( outputFieldWidth, '\b' );
    std::string name;
    int i = 0;
    std::cout << "Files removed:" << std::setw( outputFieldWidth ) << 0 << std::flush;
    static const size_t outputPeriod = 1000;
    do {
      for( size_t j = 0; j < outputPeriod && name != MARKER; j++ ) {
        mx_read[ i ].lock(); // << SYNC
        name = buffer[ i ];
        mx_proc[ i ].unlock(); // << SYNC
        i ^= 1;
        count += static_cast< size_t >( Remove( name ) );
      }
      std::cout << backspaces << std::setw( outputFieldWidth ) << count << std::flush;
    } while( name != MARKER );
    std::cout << backspaces << std::setw( outputFieldWidth ) << count << std::endl;
  } );

  std::string name;
  int i = 0;
  do {    
    name = NextFileName();
    mx_proc[ i ].lock(); // << SYNC
    buffer[ i ] = name;
    mx_read[ i ].unlock(); // << SYNC
    i ^= 1;
  } while( not name.empty() );
  mx_proc[ i ].lock(); // << SYNC
  buffer[ i ] = MARKER;
  mx_read[ i ].unlock(); // << SYNC

  eraser.join();
  return count;
}

size_t DirCtrl::RemoveAllMT( size_t _nThreads ) {
  if( _nThreads == 0 )
    return RemoveAll();
  else if( _nThreads == 1 )
    return RemoveAllT();

  struct Context {
    std::thread thread;
    std::string buffer;
    std::mutex mx_read;
    std::mutex mx_proc;
    Context() {
      mx_read.lock();
    }
  };

  std::vector< Context > ctx( _nThreads );
  std::atomic_uint count( 0 );

  auto eraserLambda = [ this, &count ]( Context& _rCtx ){
    std::string name;
    int i = 0;
    static const size_t outputPeriod = 1000;
    do {
      for( size_t j = 0; j < outputPeriod && name != MARKER; j++ ) {
        _rCtx.mx_read.lock(); // << SYNC
        name = _rCtx.buffer;
        _rCtx.mx_proc.unlock(); // << SYNC
        i ^= 1;
        count.fetch_add( static_cast< size_t >( Remove( name ) ) );
      }
    } while( name != MARKER );
  };

  std::thread progress( [ &count ](){
    static const size_t outputFieldWidth = 12;
    std::string backspaces( outputFieldWidth, '\b' );
    std::cout << "Files removed:" << std::setw( outputFieldWidth ) << 0 << std::flush;
    while( true ) {
      std::this_thread::sleep_for( std::chrono::milliseconds( 500 ) );
      std::cout << backspaces << std::setw( outputFieldWidth ) << count << std::flush;
    }
  });

  progress.detach();

  for( size_t thread_i = 0; thread_i < _nThreads; thread_i++ )
    ctx[ thread_i ].thread = std::thread( eraserLambda, std::ref( ctx[ thread_i ] ) );

  std::string name;
  size_t i = 0;
  do {
    name = NextFileName();
    ctx[ i ].mx_proc.lock(); // << SYNC
    ctx[ i ].buffer = name;
    ctx[ i ].mx_read.unlock(); // << SYNC
    if( ++i == _nThreads )
      i = 0;
  } while( not name.empty() );

  for( size_t thread_i = 0; thread_i < _nThreads; thread_i++ ) {
    ctx[ thread_i ].mx_proc.lock(); // << SYNC
    ctx[ thread_i ].buffer = MARKER;
    ctx[ thread_i ].mx_read.unlock(); // << SYNC
    ctx[ thread_i ].thread.join();
  }

  // Waiting last progress update
  std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
  std::cout << std::endl;

  return count;
}

size_t DirCtrl::CreateMadFiles( size_t _n, size_t _bs ) {
  std::ifstream rand;
  std::ios_base::iostate mask_if = rand.exceptions() | std::ios::failbit;
  rand.exceptions( mask_if );
  rand.open( "/dev/urandom", std::ifstream::in );

  std::cout << "Creating " << _n << " files:  0%" << std::flush;

  static const size_t outputPeriod = 1000;
  char* buffer = new char[ _bs ];

  std::ofstream ofs;
  std::ios_base::iostate mask_of = ofs.exceptions() | std::ios::failbit;
  ofs.exceptions( mask_of );
  size_t i;
  try {
    for( i = 0; i < _n; ) {
      size_t localPeriod = (_n - i >= outputPeriod ? outputPeriod : _n - i);
      for( size_t j = 0; j < localPeriod; j++, i++ ) {
        std::string filePath = path + "/file" + std::to_string( i ) + ".txt";
        ofs.open( filePath, std::ofstream::out | std::ofstream::app );
        rand.read( buffer, _bs );
        ofs.write( buffer, _bs );
        ofs.close();
      }
      std::cout << "\b\b\b\b" << std::setw( 3 ) << i*100/_n << "%" << std::flush;
    }
  }
  catch( std::ios_base::failure& ex ) {
    std::cerr << "Error while creating file #" << i << ": " << ex.what() << std::endl;
  }
  std::cout << "\b\b\b\b100%" << std::endl;
  rand.close();
  delete[] buffer;
  return i;
}
