#ifndef DIRCTLR_H
#define DIRCTLR_H

#include <string>

typedef struct __dirstream DIR;
struct dirent;

class DirCtrl {
  static const std::string MARKER;

  std::string path;
  DIR* dp;
  dirent* ep;

public:
  DirCtrl( const std::string& _crPath );
  ~DirCtrl();

  bool Exists() const;
  bool TryMake() const;
  bool Open();
  bool NextFile();
  std::string NextFileName();
  void Reset();
  bool AtEnd() const;
  std::string CurrentName() const;
  bool RemoveCurrent();
  bool Remove( const std::string& _crName );
  size_t RemoveAll();
  size_t RemoveAllT();
  size_t RemoveAllMT( size_t _nThreads );
  size_t CreateMadFiles( size_t _n, size_t _bs = 4096 );
};

#endif // DIRCTLR_H

