#include <iostream>

#include "ArgParser.h"
#include "DirCtlr.h"

int main( int _argc, char* _argv[] ) {

  std::string dirPath       = "";
  size_t      runInThreads  = 0;
  size_t      filesToCreate = 0;
  size_t      filesSize     = 4096;
  bool        eraseAllFiles = false;
  bool        showHelp      = false;

  ArgParser ap;
  ap.AddArg( 'h', ArgParser::NO_ARG, "show options discription;",
             [ &ap, &showHelp ]( const std::string&     ){ showHelp = true; ap.PrintHelp(); } );
  ap.AddArg( 'd', ArgParser::HAS_ARG, "set directory as argument, default: work directory;",
             [ &dirPath       ]( const std::string& _cr ){ dirPath = _cr; } );
  ap.AddArg( 't', ArgParser::HAS_ARG,  "use separate threads (count is argument) for delete-operations;",
             [ &runInThreads  ]( const std::string& _cr ){ runInThreads = atoi( _cr.c_str() ); } );
  ap.AddArg( 'c', ArgParser::HAS_ARG, "create N (argument) files with random content;",
             [ &filesToCreate ]( const std::string& _cr ){ filesToCreate = atoi( _cr.c_str() ); } );
  ap.AddArg( 'e', ArgParser::NO_ARG,  "erase all files in directory;",
             [ &eraseAllFiles ]( const std::string&     ){ eraseAllFiles = true; } );
  ap.AddArg( 's', ArgParser::HAS_ARG, "size of files created in bytes (default: 4096);",
             [ &filesSize     ]( const std::string& _cr ){ filesSize = atoi( _cr.c_str() ); } );

  if( not ap.Parse( _argc, _argv ) )
    return EXIT_FAILURE;

  if( eraseAllFiles and filesToCreate != 0 ) {
    std::cerr << "Incompatible options -c and -e." << std::endl;
    return EXIT_FAILURE;
  }

  if( dirPath.empty() )
    dirPath = ".";
  if( eraseAllFiles || filesToCreate != 0 ) {
    char* fullPath = realpath( dirPath.c_str(), NULL );
    std::cout << "Directory: " << fullPath << "\n";
    free( fullPath );
  }

  DirCtrl dc( dirPath );

  if( eraseAllFiles ) {
    if( not dc.Exists() ) {
      std::cerr << "No such directory: " << dirPath << std::endl;
      return EXIT_FAILURE;
    }
    if( not dc.Open() )
      return EXIT_FAILURE;

    dc.RemoveAllMT( runInThreads );
  }
  else if( filesToCreate != 0 ) {
    if( not dc.Exists() && not dc.TryMake() )
      return EXIT_FAILURE;
    size_t n = dc.CreateMadFiles( filesToCreate, filesSize );
    if( n != filesToCreate )
      return EXIT_FAILURE;
  }
  else if( not showHelp ) {
    if( not dc.Exists() ) {
      std::cerr << "No such directory: " << dirPath << std::endl;
      return EXIT_FAILURE;
    }
    if( not dc.Open() )
      return EXIT_FAILURE;

    size_t count = 0;
    while( dc.NextFile() )
        count++;

    std::cout << "Files count: " << count << std::endl;
  }
  return EXIT_SUCCESS;
}

