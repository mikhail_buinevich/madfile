TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

TARGET = mf

SOURCES += main.cpp \
    DirCtlr.cpp

LIBS += -lpthread

HEADERS += \
    ArgParser.h \
    DirCtlr.h

DISTFILES += \
    Makefile

